# -*- coding: utf-8 -*-
class ArabicCharSet:
    def __init__(self):
        self.consonants = frozenset(['ا', 'ب', 'ت', 'ث', 'ج', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'س', 'ش', 'ص',
                                     'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ك', 'ل', 'م', 'ن', 'ه', 'و', 'ي'])
        self.short_vowels = frozenset(['َ', 'ُ', 'ِ'])
        self.long_vowels = frozenset(['ا', 'و', 'ي'])
        self.shamsi = frozenset(['ﺕ', 'ﺙ', 'ﺩ', 'ﺫ', 'ﺭ', 'ﺯ', 'ﺱ', 'ﺵ', 'ﺹ', 'ﺽ', 'ﻁ', 'ﻅ', 'ﻝ', 'ﻥ'])
        self.qamari = frozenset(['ء', 'ﺏ', 'ﺝ', 'ﺡ', 'ﺥ', 'ﻉ', 'ﻍ', 'ﻑ', 'ﻕ', 'ﻙ', 'ﻡ', 'ﻭ', 'ﻱ', 'ه'])
        self.special_letters = {
            'alif': 'ا',
            'skoon': 'ْ',
            'shaddah': 'ّ',
            'hamzah': 'ء',
            'lam': 'ل',
            'fatha': 'َ',
            'waw': 'و',
            'alif_maqsurah': 'ى',
            'tanwin_fathah': 'ً',
            'taa_marbuta': 'ة',
            'nun': 'ن',
        }
        self.phoneme = {
            'ب': 'b', 'ت': 't', 'د': 'd', 'ط': 't`', 'ض': 'd`', 'ك': 'k', 'ج': 'g', 'ء': '#',
            'ق': 'q', 'ب': 'p', 'ف': 'f', 'ث': 'T', 'ذ': 'D', 'ظ': 'D`', 'س': 's', 'ز': 'z',
            'ص': 's`', 'ش': 'S', 'ج': 'Z', 'خ': 'x', 'غ': 'G', 'ح': 'X/', 'ع': '#`', 'ه': 'h',
            'م': 'm', 'ن': 'n', 'ر': 'r', 'ل': 'l', 'ل': 'l`', 'و': 'w', 'ي': 'j',
            'َ': 'a', 'ُ': 'u', 'ِ': 'i', 'ا': 'A', 'و': 'U', 'ي': 'I',
        }
        self.tanween = {  # key: tanween, value: short vowel
            'ً': 'َ',  # fathah
            'ٌ': 'ُ',  # dammah
            'ٍ': 'ِ',  # kasrah
        }
