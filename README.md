# Rule based Arabic Grapheme-to-Phoneme

# Ref Papers
- "Automatic grapheme-to-phoneme conversion of Arabic text"
    - Author: B. Al-Daradkah and B. Al-Diri
    - 2015 Science and Information Conference (SAI), London, 2015, pp. 468-473.