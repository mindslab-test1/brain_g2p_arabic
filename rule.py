# -*- coding: utf-8 -*-
from char_set import ArabicCharSet
import re
import glob
import os


class PhonologyRules:
    def __init__(self, sentence: str, charset: ArabicCharSet):
        # split into arabic characters and non-arabic characters (split as regex '\b'(word boundary))
        sentence = re.sub(r'([\u0600-\u06FF]+)', r'{\1}', sentence)
        reg = re.compile(r'([\u0600-\u06FF]+|[^\u0600-\u06FF]+)')
        self.sentence = list(map(list, reg.findall(sentence)))  # sentence = list of word / word = list of char
        self.charset = charset
        self.rule_seq = iter([self._skoon_rule, self._shadda_rule, self._hamzah_rule, self._alif_rule,
                              self._taa_rule, self._lam_rule, self._tanween_rule, self._vowels_rule,
                              self._last_word_rule, self._after_rule])

    def list2sentence(self):
        s = list()
        flag = False
        for w in self.sentence:
            if flag:
                s.append(' '.join(w))
                flag = False
                continue
            s.append(''.join(w))
            if len(w) > 0 and w[-1] == '{':
                flag = True
        return ''.join(s)

    def __iter__(self):
        return self

    def __next__(self):
        self.rule_seq.__next__()()
        return self.list2sentence()

    def next_all(self):
        for func in self.rule_seq:
            func()
        return self.list2sentence()

    def _skoon_rule(self):  # Rule01
        harakeh = frozenset(set([self.charset.special_letters['skoon'], self.charset.special_letters['shaddah']])
                            | set(self.charset.short_vowels))  # Skoon, Shaddah and short vowels
        for i, w in enumerate(self.sentence):
            for j, c in enumerate(w):
                if c in self.charset.consonants and not(j < len(w) - 1 and self.sentence[i][j + 1] in harakeh):
                    pass
                    # raise ValueError('Wrong sentence with Skoon(Sukun) Rule at {} in {}'.format(w, self.sentence))

    def _shadda_rule(self):  # Rule02
        for i, w in enumerate(self.sentence):
            for j, c in enumerate(w):
                if c in self.charset.consonants and j < len(w) - 1 \
                        and self.sentence[i][j + 1] == self.charset.special_letters['shaddah']:
                    self.sentence[i][j - 1] = c

    def _hamzah_rule(self):  # Rule03
        letters = frozenset(['ؤ‎ ', 'ئ', 'أ', 'إ‎ ', 'آ'])  # need to check
        for i, w in enumerate(self.sentence):
            for j, c in enumerate(w):
                if c in letters:
                    self.sentence[i][j] = self.charset.special_letters['hamzah']

    def _alif_rule(self):
        if len(self.sentence) > 0 and len(self.sentence[0]) > 0 and\
                self.sentence[0][0] == self.charset.special_letters['alif']:  # Rule04
            self.sentence[0][0] = self.charset.special_letters['hamzah']

        for i, w in enumerate(self.sentence[1:]):  # Rule05
            if len(w) > 1 and w[0] == self.charset.special_letters['alif'] \
                    and w[1] == self.charset.special_letters['lam']:
                del self.sentence[i][0]

        for i, w in enumerate(self.sentence):  # Rule06
            for j, c in enumerate(w):
                if c == self.charset.special_letters['alif_maqsurah']:
                    self.sentence[i][j] = self.charset.special_letters['fatha']

        for i, w in enumerate(self.sentence):  # Rule07
            if len(w) > 1 and w[-1] == self.charset.special_letters['alif'] \
                    and w[-2] == self.charset.special_letters['waw']:
                del self.sentence[i][-1]

        for i, w in enumerate(self.sentence):  # Rule08
            for j, c in enumerate(w):
                if len(w) > 1 and c == self.charset.special_letters['alif'] \
                        and w[j - 1] == self.charset.special_letters['tanwin_fathah']:
                    del self.sentence[i][j]

    def _taa_rule(self):
        vowels = self.charset.short_vowels | self.charset.long_vowels
        for i, w in enumerate(self.sentence):  # Rule09
            if not((len(w) > 0 and w[-1] == self.charset.special_letters['taa_marbuta']) or
                   (len(w) > 1 and w[-2] == self.charset.special_letters['taa_marbuta'] and w[-1] in vowels)):
                pass
                # raise ValueError('Wrong sentence with Taa Marbuta Rule')

        for i, w in enumerate(self.sentence):  # Rule10
            if len(w) > 1 and w[-2] == self.charset.special_letters['taa_marbuta'] and w[-1] not in vowels:
                self.sentence[i][-2] = 'ه'  # need to check character

    def _lam_rule(self):
        for i, w in enumerate(self.sentence):  # Rule11
            for j, c in enumerate(w):
                if j < len(w) - 1 and c == self.charset.special_letters['lam'] and w[j + 1] in self.charset.shamsi:
                    del self.sentence[i][j]

        for i, w in enumerate(self.sentence):  # Rule12
            for j, c in enumerate(w):
                if j < len(w) - 1 and c == self.charset.special_letters['lam'] and w[j + 1] in self.charset.qamari:
                    self.sentence[i][j] = self.charset.phoneme[c]

    def _tanween_rule(self):
        for i, w in enumerate(self.sentence):
            if len(w) > 0 and w[-1] in self.charset.tanween.keys():  # Rule 13, 14, 15
                short_vowel = self.charset.tanween[w[-1]]
                w[-1] = short_vowel
                w.append(self.charset.special_letters['nun'])

    def _vowels_rule(self):
        for i, w in enumerate(self.sentence):
            for j, c in enumerate(w):
                if j > 1 and c in self.charset.long_vowels and w[j - 1] in self.charset.short_vowels:
                    self.sentence[i][j] = self.charset.phoneme[c]
                    del self.sentence[i][j - 1]

    def _last_word_rule(self):
        if len(self.sentence) > 0 and len(self.sentence[-1]) > 0 and self.sentence[-1][-1] in self.charset.short_vowels:
            del self.sentence[-1][-1]

    def _after_rule(self):
        for i, w in enumerate(self.sentence):
            for j, c in enumerate(w):
                if c in self.charset.phoneme.keys():
                    self.sentence[i][j] = self.charset.phoneme[c]


if __name__ == '__main__':
    glob_path = ''
    charset = ArabicCharSet()
    files = glob.glob(glob_path, recursive=True)
    for f in files:
        file_name = os.path.splitext(f)[0]
        file_ext = os.path.splitext(f)[1]
        with open(f, 'r', encoding='utf-8') as read_file:
            file_lines = read_file.readlines()
        file_lines = [PhonologyRules(line, charset).next_all() for line in file_lines]
        with open(file_name + '_output' + file_ext, 'w', encoding='utf-8') as write_file:
            write_file.writelines(file_lines)
